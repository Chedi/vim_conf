set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Bundle 'gmarik/vundle'
Bundle 'tpope/vim-fugitive'
filetype plugin indent on

Bundle 'bling/vim-airline'
set laststatus=2
let g:airline_powerline_fonts = 1
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline
let g:airline#extensions#tabline#enabled = 1

function! AirlineInit()
	let g:airline_section_a = airline#section#create(['mode',' ','branch'])
	let g:airline_section_b = airline#section#create_left(['ffenc','hunk','%f'])
	let g:airline_section_c = airline#section#create(['filetype'])
	let g:airline_section_x = airline#section#create(['%P'])
	let g:airline_section_y = airline#section#create(['%B'])
	let g:airline_section_z = airline#section#create_right(['%l','%c'])
endfunction
autocmd VimEnter * call AirlineInit()

Bundle 'scrooloose/nerdtree'

set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

Bundle 'davidhalter/jedi-vim'
Bundle 'klen/python-mode'

"   Rope autocomplete
" g     Rope goto definition
" d     Rope show documentation
" f     Rope find occurrences
" b     Set, unset breakpoint (g:pymode_breakpoint enabled)
" [[            Jump on previous class or function (normal, visual, operator  modes)
" ]]            Jump on next class or function (normal, visual, operator modes)
" [M            Jump on previous class or method (normal, visual, operator modes)
" ]M            Jump on next class or method (normal, visual, operator modes)

let g:pymode_rope = 0
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'

let g:pymode_lint = 0
let g:pymode_lint_checker = "pyflakes,pep8"
let g:pymode_lint_write = 0
let g:pymode_virtualenv = 1
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_key = 'b'

let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all

let g:pymode_folding = 0

Bundle 'majutsushi/tagbar'
Bundle 'bling/vim-bufferline'
Bundle 'mattn/emmet-vim'

nmap <F8> :TagbarToggle<CR>
map <C-n> :NERDTreeToggle<CR>
map gn :bn<cr>
map gp :bp<cr>
map gd :bd<cr>

set hidden
set nowrap 
set tabstop=4 
set backspace=indent,eol,start 
set autoindent 
set copyindent 
set number     
set shiftwidth=4 
set shiftround 
set showmatch 
set ignorecase 
set smartcase 
set smarttab
set hlsearch 
set incsearch 
set nobackup
set noswapfile
        
nmap l :set list!
set autochdir
set tabstop=4
set shiftwidth=4
set expandtab

se nu
:map <C-a> GVgg
:map <C-o> :e . <Enter>
:map <C-z> :w <Enter>
:map <C-c> y
:map <C-v> p
:map <C-x> d
:map <C-u> u
:map <C-t> :tabnew <Enter>
:map <C-i> >>
:map <C-q> :close <Enter>
:map <C-Q> :q! <Enter>
:map <C-f> /
:map <F3> n
:map <C-h> :%s/
:map <S-t> vat
:map <S-T> vit
:map <S-{> vi{
:map <S-(> vi(
:map <S-[> vi[

Bundle "MarcWeber/vim-addon-mw-utils"
Bundle "tomtom/tlib_vim" 
Bundle "garbas/vim-snipmate"
Bundle "honza/vim-snippets"
